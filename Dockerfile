FROM golang:alpine as builder

RUN apk add --no-cache git

WORKDIR /src
COPY go.* ./
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=0 GOOS=linux go build .

FROM scratch
COPY mime.types /etc/mime.types
COPY assets /assets
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /src/custom-errors /custom-errors
ENTRYPOINT ["/custom-errors"]
